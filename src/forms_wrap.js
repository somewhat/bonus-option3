import React, { Component } from 'react';


class FormsWrap extends Component {
   handleClick = (e) => {
    if(e.target.attributes.getNamedItem('id').value === 'tab-1'){
		document.getElementById('tab-2').classList.remove('active');
		document.getElementById('tab-1').classList.add('active');
		document.getElementById('tab-highlight').classList.remove('right');
		document.getElementById('tab-2-wrap').classList.remove('active');
		setTimeout(() => {
  			document.getElementById('tab-1-wrap').classList.add('active');
		}, 500);
		
	}else {
		document.getElementById('tab-1').classList.remove('active');
		document.getElementById('tab-2').classList.add('active');
		document.getElementById('tab-highlight').classList.add('right');
		document.getElementById('tab-1-wrap').classList.remove('active');
		setTimeout(() => {
  			document.getElementById('tab-2-wrap').classList.add('active');
		}, 500);
	}
  }
  render() {
    return (
      <div className="form-wrap clearfix">
        	<div className="form-tab">
            	<div className="form-tab-highlight" id="tab-highlight"></div>
            	<div id="tab-1" className="form-tab-1 w-50 active" onClick={this.handleClick}>For Employers</div>
                <div id="tab-2" className="form-tab-2 w-50" onClick={this.handleClick}>For Employees</div>
            </div>
            <div className="form-1 active" id="tab-1-wrap">
            	<div className="w-47"><input type="text" className="text-box" placeholder="Name*" /></div>
                <div className="w-6">&nbsp;</div>
                <div className="w-47"><input type="text" className="text-box" placeholder="Company name" /></div>
                <div className="w-47"><label>&nbsp;</label><input type="text" className="text-box" placeholder="Email address*" /></div>
                <div className="w-6">&nbsp;</div>
                <div className="w-47"><label>How many employess are at your company?</label><select className="select-box"><option>Choose amount</option><option>0-500</option><option>500+</option></select></div>
                <div className="w-100"><input type="submit" value="Request a demo" className="submit-btn" /></div>
            </div>
            <div className="form-2" id="tab-2-wrap">
            	<p>If you'd like your employer to offer MoneyLion Bonus, <br />share this page with someone in your company's <br />HR department.</p>
            	<div className="w-47"><input type="text" className="text-box" placeholder="Your name*" /></div>
                <div className="w-6">&nbsp;</div>
                <div className="w-47"><input type="text" className="text-box" placeholder="HR contact*" /></div>
                <div className="w-47"><input type="text" className="text-box" placeholder="Your email address*" /></div>
                <div className="w-6">&nbsp;</div>
                <div className="w-47"><input type="text" className="text-box" placeholder="HR contact email address*" /></div>
                <div className="w-100"><input type="submit" value="Share this page" className="submit-btn" /></div>
            </div>
        </div>
    );
  }
}


export default FormsWrap;
